<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FertilizerReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fertilizer_receipt', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("m_ship_id")->nullable();
            $table->integer("no_voy")->nullable();
            $table->integer("m_charge_from_id")->nullable();
            $table->date('date_arrival')->nullable();
            $table->date('date_departure')->nullable();
            $table->float("tonage_bl")->nullable();
            $table->float("tonage_ds")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fertilizer_receipt');
    }
}
