<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FertilizerLoading extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fertilizer_loading', function (Blueprint $table) {
            $table->increments('id');
            $table->string("posto")->nullable();
            $table->date("date")->nullable();
            $table->integer("vendors_id")->nullable();
            $table->integer("m_destination_id")->nullable();
            $table->float("capital")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fertilizer_loading');
    }
}
